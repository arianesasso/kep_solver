Welcome to kep_solver's documentation!
======================================

.. automodule:: kep_solver

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   source/usage
   source/formats.rst
   source/terms.rst
   source/kep_solver

.. mdinclude:: ../README.md

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
