# Notebooks

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/wpettersson%2Fkep_solver/HEAD)

This folder contains notebooks for kep\_solver. You can download and run these in your own installation of Jupyter Notebook if you wish, or you can utilise [MyBinder](https://mybinder.org/v2/gl/wpettersson%2Fkep_solver/HEAD).

## Match run

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/wpettersson%2Fkep_solver/HEAD?labpath=notebooks%2FMatch%20Run.ipynb)

This notebook demonstrates how to use kep\_solver to design kidney exchange programmes with different objectives and properties and then run instances through them to see how the selected transplants are affected.

## Statistical Analysis

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/wpettersson%2Fkep_solver/HEAD?labpath=notebooks%2FStatistical%20Analysis.ipynb)

This notebook demonstrates how to examine a set of kidney exchange instances for various statistical properties or relationships, and then use this information to create random instance generators.
