{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "16df912a",
   "metadata": {},
   "source": [
    "## Expectations from this notebook\n",
    "\n",
    "This notebook is going to give some examples of statistical analyses that can be performed on data from a set of KEP instances. It is important to note a few things.\n",
    "\n",
    "1. The data being used has been randomly generated for testing purposes. It does not match any known population, and should not be used to arrive at any conclusions for real-world kidney exchange programmes.\n",
    "2. The outputs from statistical analyses in general, including the ones in this notebook, should not be used without considering whether the analysis technique is appropriate, and whether the conclusion is believable.\n",
    "\n",
    "To properly consider the outputs from the analyses, it is important to have an understanding of what the various parameters mean. For a brief introduction into the parameters used, see [the kep_solver documentation](https://kep-solver.readthedocs.io/en/latest/source/terms.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4a0fd654",
   "metadata": {},
   "source": [
    "First we're just going to install the relevant packages. For our analyses, we install `statsmodels`, and for some visualisations we use `matplotlib`. We also pull in `numpy` and `pandas` so we can refer to certain parts of them. Note that `kep_solver` itself doesn't do any statistical analyses - plenty of other packages exist for such purposes so we use them, and you can use your preferred statistics package instead."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "43bfd825",
   "metadata": {},
   "outputs": [],
   "source": [
    "!pip install kep_solver statsmodels matplotlib numpy pandas\n",
    "import json\n",
    "\n",
    "from kep_solver.fileio import read_json\n",
    "from kep_solver.entities import InstanceSet, BloodGroup\n",
    "import kep_solver.generation as generation\n",
    "\n",
    "import statsmodels.api as sm\n",
    "import matplotlib.pyplot as plt\n",
    "# Some extra imports to make things look nicer\n",
    "import matplotlib.ticker as mticker\n",
    "import matplotlib.colors as mcolors\n",
    "\n",
    "import numpy\n",
    "import pandas"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "25b1d56c",
   "metadata": {},
   "source": [
    "We now can build an `InstanceSet`. This is a part of the `kep_solver` package, and is a representation of a set of instances. We will see later that it can perform some relevant calculations over the set of instances."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8587a78f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# get the data\n",
    "filenames = [f\"medium-{i}.json\" for i in range(1,11)]\n",
    "instances = [read_json(\"../tests/test_instances/\" + filename) for filename in filenames]\n",
    "instances = InstanceSet(instances)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "daadbcb6",
   "metadata": {},
   "source": [
    "Details of the donors, as a complete table, can be extracted. This is returned as a `pandas.DataFrame` object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e787adfd",
   "metadata": {},
   "outputs": [],
   "source": [
    "donors = instances.donor_details()\n",
    "print(donors)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a5a0c6b7",
   "metadata": {},
   "source": [
    "Blood group distributions will come up a few times, so we make a function to plot such distributions, and then plot the distribution of blood groups amongst donors."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cef04c3e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# make a blood group distribution function\n",
    "def plot_bloodgroups(column, axis=None, title=\"Blood group distribution\"):\n",
    "    plot = column.value_counts().plot(ax=axis,\n",
    "                                      kind='bar',\n",
    "                                      xlabel='Blood group',\n",
    "                                      ylabel='Frequency',\n",
    "                                      title=title)\n",
    "    plot.tick_params(rotation=0)\n",
    "\n",
    "plot_bloodgroups(donors[\"donor_bloodgroup\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "727492e3",
   "metadata": {},
   "source": [
    "We will create \"configurations\" that can be used to create random entity generators. These configurations are meant to be stored as JSON files, so we use `to_json` function from `pandas` to create a JSON string. However, we then turn this back into a dictionary using the `json.loads()` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6de6e1b7",
   "metadata": {},
   "outputs": [],
   "source": [
    "generic_donor_distribution = json.loads(donors[\"donor_bloodgroup\"].value_counts(normalize=True).to_json())\n",
    "print(generic_donor_distribution)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5b0a230d",
   "metadata": {},
   "source": [
    "We now consider our first possible correlation between variables. The question we pose is: Does the distribution of donor blood groups depend on the blood group of their paired recipient? Looking at this data, it does look like a correlation may exist as, if a recipient has blood group AB, then it seems unlikely that a donor will have blood group B. In a real analysis, you should take note of the population size (i.e., there are only 3 donors who are paired with recipients who have blood group AB) and use that to drive your decision. In this notebook, we are simply demonstrating what is possible, and so ignore the population sizes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5a9eddfe",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axs = plt.subplots(2, 2, tight_layout=True)\n",
    "donor_config = {\"Generic\": generic_donor_distribution}\n",
    "for bloodgroup, ax in zip(BloodGroup.all(), fig.axes):\n",
    "    filtered = donors[donors['paired_recipient_bloodgroup'] == bloodgroup]\n",
    "    plot_bloodgroups(filtered[\"donor_bloodgroup\"], axis = ax, title=f\"Recipient has {bloodgroup}\")\n",
    "    donor_config[str(bloodgroup)] = json.loads(filtered[\"donor_bloodgroup\"].value_counts(normalize=True).to_json())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "99bf2afa",
   "metadata": {},
   "source": [
    "When splitting out donor blood groups by the blood group of their paired recipient, we lost all non-directed donors, so lets look at those now."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "999ac8d1",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_bloodgroups(donors[donors[\"NDD\"] == True][\"donor_bloodgroup\"], title=f\"Non-directed donors\")\n",
    "ndd_bloodgroup_dist = json.loads(donors[donors[\"NDD\"] == True][\"donor_bloodgroup\"].value_counts(normalize=True).to_json())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "45b284f2",
   "metadata": {},
   "source": [
    "Now we want to look at recipients, but first, we have to calculate the `compatibility_chance`. For a given recipient R, the `compatibility_chance` of R is the ratio between `number of distinct donors who can donate to R` and `number of donors who are blood group compatible with R and appear in a common instance with R`. This is calculated when we extract recipient_details."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "647dea4f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# This calculates compatibility_chance for each recipient.\n",
    "\n",
    "recipients = instances.recipient_details(calculate_compatibility=True)\n",
    "print(recipients)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2ebf50a8",
   "metadata": {},
   "source": [
    "Since we have some more data, let's look at a correlation matrix for the recipients. As a reminder, a correlation matrix has values between -1.0 and 1.0, where a value of -1.0 indicates a negative linear correlation, 0.0 indicates no linear correlation, and 1.0 indicates a positive linear correlation. Again, we also highlight that such matrices make many assumptions (such as linear relationships, but also on the underlying distribution of each variable) and so one should consider any implications of any correlations that are utilised.\n",
    "\n",
    "We see from the below that a higher cPRA is perhaps linked to having a lower compatibility chance (which makes sense from the definitions of cPRA and compatibility chance) and that having a blood group compatible donor might be linked to having a higher cPRA (which makes sense, as a recipient with a blood group compatible donor and a low cPRA is more likely to have a transplant arranaged without entering a KEP)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c45b3aa2",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(recipients.corr())\n",
    "labels = [\"cPRA\", \"Compatibility\", \"#Donors\", \"ABO Comp. Donor\"]\n",
    "fig, ax = plt.subplots(figsize=(10,5))\n",
    "norm = mcolors.Normalize(vmin=-1, vmax=1)\n",
    "cax = ax.matshow(recipients.corr(), cmap=plt.cm.get_cmap('PuBu'), norm=norm)\n",
    "fig.colorbar(cax)\n",
    "ticks_loc = ax.get_xticks().tolist()\n",
    "ax.xaxis.set_major_locator(mticker.FixedLocator(ticks_loc))\n",
    "ax.set_xticklabels([''] + labels + [''])\n",
    "\n",
    "# fixing yticks with \"set_yticks\"\n",
    "ticks_loc = ax.get_yticks().tolist()\n",
    "ax.yaxis.set_major_locator(mticker.FixedLocator(ticks_loc))\n",
    "ax.set_yticklabels([''] + labels + ['']);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2e168184",
   "metadata": {},
   "source": [
    "We can now look at some individual properties of the recipients, starting with the distribution of number of donors. Again, we are building configuration objects for use later as well."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9253618c",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot = recipients[\"num_donors\"].value_counts().plot(kind='bar',\n",
    "                                  xlabel='Number of donors',\n",
    "                                  ylabel='Frequency',\n",
    "                                  title=\"Donors per recipient\")\n",
    "plot.tick_params(rotation=0)\n",
    "donor_count_config = {}\n",
    "for num_donors, probability in recipients[\"num_donors\"].value_counts(normalize=True).to_dict().items():\n",
    "    donor_count_config[num_donors] = probability"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f9df068d",
   "metadata": {},
   "source": [
    "The blood group distribution of the recipients is also relevant, and easy to examine."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d9c0c773",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_bloodgroups(recipients[\"recipient_bloodgroup\"], title=\"Recipient Bloodgroup distribution\")\n",
    "recipient_bloodgroup_distribution = json.loads(recipients[\"recipient_bloodgroup\"].value_counts(normalize=True).to_json())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2ab622dc",
   "metadata": {},
   "source": [
    "Next we look at the distribution of cPRA. Since this can take many values, we use a histogram to view the data. Note that we have specified the bins exactly, you can also just specify a certain number of bins."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f4181cd0",
   "metadata": {},
   "outputs": [],
   "source": [
    "cPRA_histogram_bins = [0.05 * index for index in range(0, 21)]\n",
    "plot = recipients[\"cPRA\"].plot(kind='hist',\n",
    "                               xlabel='cPRA',\n",
    "                               ylabel='Frequency',\n",
    "                               title=\"cPRA Distribution\",\n",
    "                               bins=cPRA_histogram_bins,\n",
    "                               edgecolor=\"black\"\n",
    "                               )\n",
    "plot.tick_params(rotation=0)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "44b6e29e",
   "metadata": {},
   "source": [
    "Recall that the correlation matrix indicated a possible link between having a blood group compatible donor and cPRA. We begin by separating recipients into those who have a blood group compatible donor, and those who don't, and create two separate histograms on the same axes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a8fdbdf4",
   "metadata": {},
   "outputs": [],
   "source": [
    "compatible = recipients[recipients[\"has_abo_compatible_donor\"] == True][\"cPRA\"]\n",
    "incompatible = recipients[recipients[\"has_abo_compatible_donor\"] == False][\"cPRA\"]\n",
    "fig, ax = plt.subplots()\n",
    "cpra_results = ax.hist([compatible, incompatible], edgecolor=\"black\", density=True, bins=cPRA_histogram_bins,\n",
    "                       label=[\"Has ABO compatible donor\", \"Does not have an ABO compatible donor\"])\n",
    "ax.set_xlabel(\"cPRA\")\n",
    "ax.set_ylabel(\"Frequency\")\n",
    "ax.set_title(\"cPRA distributions\")\n",
    "ax.legend();"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ca59b779",
   "metadata": {},
   "source": [
    "From the plot, it seems likely that there is some link, so we're going to extract this data into configuration for a generator we will use later. We do this by utilising the density-based results from calling `hist()`. We then create the configurations. These are created by capturing, for each bin, the lower and upper bound as well as the probability density of said bin.\n",
    "\n",
    "Note that we have to slightly shift the upper bound of the last bin. While `matplotlib` ensures that all bins have a strict upper bound except the last, our `kep_solver` software will expect to have a strict upper bound on the last bin. To ensure that we can still get cPRA values equal to 1.0, we have to push this upper bound. The `kep_solver` software will itself ensure that a cPRA above 1.0 will not be generated."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "26c30fbc",
   "metadata": {},
   "outputs": [],
   "source": [
    "compat_densities = []\n",
    "incompat_densities = []\n",
    "for ind, bin_lower in enumerate(cPRA_histogram_bins[:-1]):\n",
    "    bin_upper = cPRA_histogram_bins[ind+1]\n",
    "    # plt.hist returns a probability density such that the integral over all probabilities is equal to one.\n",
    "    # We just want sum of probabilities to equal one, so multiply each probability by the bin width\n",
    "    probability_compatible = cpra_results[0][0][ind] * (bin_upper - bin_lower)\n",
    "    probability_incompatible = cpra_results[0][0][ind] * (bin_upper - bin_lower)\n",
    "    # We have to slightly tweak the upper bound of the last bin. matplotlib makes the last upper bound closed,\n",
    "    # while kep_solver expects it to be open\n",
    "    if ind == len(cPRA_histogram_bins) - 2:\n",
    "        bin_upper += 1e-6\n",
    "    compat_densities.append(((bin_lower, bin_upper), probability_compatible))\n",
    "    incompat_densities.append(((bin_lower, bin_upper), probability_incompatible))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d09579fd",
   "metadata": {},
   "source": [
    "Next we look at any correlation between cPRA and compatibility chance. Again, we begin by plotting. From this, it seems plausible that there is some negative correlation, but it may be hard to model precisely."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0032ed43",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.scatter(recipients[\"cPRA\"], recipients[\"compatibility_chance\"])\n",
    "plt.xlabel(\"cPRA\")\n",
    "plt.ylabel(\"Compatibility Chance\");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9af3db15",
   "metadata": {},
   "source": [
    "We generate a linear model regardless, just to see how well it works."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4073ad63",
   "metadata": {},
   "outputs": [],
   "source": [
    "model = sm.formula.glm(\"compatibility_chance ~ cPRA\",\n",
    "                      data=recipients)\n",
    "result = model.fit()\n",
    "print(result.summary())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "af87a9c7",
   "metadata": {},
   "source": [
    "We now plot both the actual data, as well as our linear regression. While the black line is a line of best fit, we also see that it clearly is not perfect. Still, we don't have any better relationship to use, and from a medical perspective it does make sense that a higher cPRA means a lower compatibility chance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0458064f",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.scatter(recipients[\"cPRA\"], recipients[\"compatibility_chance\"], label=\"Data\")\n",
    "plt.xlabel(\"cPRA\")\n",
    "plt.ylabel(\"Compatibility Chance\");\n",
    "xseq = numpy.linspace(0, 1.0, num=100)\n",
    "\n",
    "# Plot regression line\n",
    "plt.plot(xseq, result.params['Intercept'] + result.params['cPRA'] * xseq, color=\"k\", lw=2.5, label=\"Regression\");\n",
    "plt.legend();"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "29481954",
   "metadata": {},
   "source": [
    "We can use all of the configurations we've been collecting to build generators. These generators allow us to randomly sample blood groups, donors, recipients, even instances, based on the distributions we've chosen."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "710b0fa2",
   "metadata": {},
   "outputs": [],
   "source": [
    "donor_generator = generation.DonorGenerator.from_json(donor_config)\n",
    "recipient_blood_generator = generation.BloodGroupGenerator.from_json(recipient_bloodgroup_distribution)\n",
    "donor_count_generator = generation.DonorCountGenerator.from_json(donor_count_config)\n",
    "cpra_generator = generation.CPRAGenerator.from_json({\"Compatible\": compat_densities,\n",
    "                                                     \"Incompatible\": incompat_densities})\n",
    "compat_generator = generation.CompatibilityChanceGenerator.from_json([[0, 1.01, \n",
    "                                                                       {\"function\": \n",
    "                                                                        {\"type\": \"linear\", \n",
    "                                                                         \"offset\": result.params['Intercept'],\n",
    "                                                                         \"coefficient\": result.params['cPRA']}}]])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "35a0b12c",
   "metadata": {},
   "source": [
    "The recipient generator needs a number of generators itself to function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cb7b9328",
   "metadata": {},
   "outputs": [],
   "source": [
    "recipient_generator = generation.RecipientGenerator(recipient_blood_generator,\n",
    "                                                    donor_count_generator,\n",
    "                                                    donor_generator,\n",
    "                                                    cpra_generator,\n",
    "                                                    compat_generator)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4a7d3d4e",
   "metadata": {},
   "source": [
    "An instance generator then needs to know how to create non-directed donors and recipients. Directed donors (also known as paired donors) will be constructed when their paired recipient is constructed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3c9e8207",
   "metadata": {},
   "outputs": [],
   "source": [
    "ndd_blood_generator = generation.BloodGroupGenerator.from_json(ndd_bloodgroup_dist)\n",
    "instance_generator = generation.InstanceGenerator(recipient_generator, ndd_blood_generator)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "33080090",
   "metadata": {},
   "source": [
    "Now we can `draw()` random instances. The `50` here means we want 50 recipients. We didn't specify any non-directed donors, so we get none. Note that we (probably) have more than 50 donors, as recipients have a small chance of being generated with two donors. Of course, we are sampling at random so we might actually get exactly 50 donors."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c2ffb593",
   "metadata": {},
   "outputs": [],
   "source": [
    "new_instance = instance_generator.draw(50)\n",
    "print(f\"The new instance has {len(new_instance.recipients())} recipients\")\n",
    "print(f\"The new instance has {len(new_instance.donors())} donors\")\n",
    "print(f\"The new instance has {len([donor for donor in new_instance.donors() if donor.NDD])} non-directed donors\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5ca94e97",
   "metadata": {},
   "source": [
    "If we want to have non-directed donors, we tell the `draw()` function how many to generate with a second parameter."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4280c91a",
   "metadata": {},
   "outputs": [],
   "source": [
    "new_instance = instance_generator.draw(50, 5)\n",
    "print(f\"The new instance has {len(new_instance.recipients())} recipients\")\n",
    "print(f\"The new instance has {len(new_instance.donors())} donors\")\n",
    "print(f\"The new instance has {len([donor for donor in new_instance.donors() if donor.NDD])} non-directed donors\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3d6e8974",
   "metadata": {},
   "source": [
    "Above we created all the individual generators, and then combined them. However, we can put all of the configuration into one large dictionary as follows. The following prints out a large (and messy looking) string, but this string contains all the information required to construct an instance generator. This string can be saved in a file, and shared with others so they can generate random instances with the same distributions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0686bc64",
   "metadata": {},
   "outputs": [],
   "source": [
    "recipient_generator_config = {\n",
    "    \"RecipientBloodGroupGenerator\": recipient_bloodgroup_distribution,\n",
    "    \"DonorCountGenerator\": donor_count_config,\n",
    "    \"DonorGenerator\": donor_config,\n",
    "    \"CPRAGenerator\": {\"Compatible\": compat_densities, \"Incompatible\": incompat_densities},\n",
    "    \"CompatibilityChanceGenerator\": [[0, 1.01, {\"function\": {\"type\": \"linear\", \n",
    "                                                             \"offset\": result.params['Intercept'],\n",
    "                                                             \"coefficient\": result.params['cPRA']\n",
    "                                                            }\n",
    "                                               }]]\n",
    "}\n",
    "instance_configuration = {\n",
    "    \"RecipientGenerator\": recipient_generator_config,\n",
    "    \"NDDBloodGroupGenerator\": ndd_bloodgroup_dist,\n",
    "}\n",
    "instance_generator_json = generation.InstanceGenerator.from_json(instance_configuration)\n",
    "json.dumps(instance_configuration)"
   ]
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
